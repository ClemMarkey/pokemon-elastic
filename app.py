from flask import Flask, render_template, redirect, url_for, jsonify
from elasticsearch import Elasticsearch

es = Elasticsearch(hosts="http://elastic:changeme@localhost:9200/")
app = Flask(__name__, static_folder='templates/assets')

app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route("/")
def home():
    pokemons = get_pokemons()
    return render_template("index.html", pokemons=pokemons)

@app.route("/<id>")
def pokemon(id):
    pokemon = get_pokemon(id)
    return render_template("pokemon.html", pokemon=pokemon)


def get_pokemon(id):
    res = es.get(index="pokedex", id=id)
    return res

def get_pokemons():
    res = es.search(index="pokedex", size=1000)
    return res['hits']['hits']